<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLink extends Model
{
    protected $fillable = ['user_id', 'link_id'];
    protected $primaryKey = 'user_link_id';
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    public function link()
    {
        return $this->belongsTo(Link::class, 'link_id', 'link_id');
    }
}
