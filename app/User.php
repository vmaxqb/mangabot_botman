<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = ['platform', 'ext_id'];
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    public function userLinks()
    {
        return $this->hasMany(UserLink::class, 'user_id');
    }

    public function links()
    {
        return $this->belongsToMany(Link::class, 'user_links', 'user_id', 'link_id', 'user_id', 'link_id');
    }
}
