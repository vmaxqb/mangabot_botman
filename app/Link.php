<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $fillable = ['link'];
    protected $primaryKey = 'link_id';
    public $timestamps = false;

    public function userLinks()
    {
        return $this->hasMany(UserLink::class, 'link_id');
    }
}
