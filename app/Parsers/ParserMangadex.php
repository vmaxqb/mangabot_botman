<?php

namespace App\Parsers;

use App\Parsers\Curl\CustomCurl;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Exceptions\CurlException;

class ParserMangadex extends AbstractParser
{
    public static $manifest = [
        'host'  => [
            'mangadex.org'     => 'https://mangadex.org/title/<ID>/<NAME>',
        ],
        'type'  => 'manga',
    ];

    public static function verifyLink(string $link): ?string
    {
        $link = trim($link);

        if (preg_match("/^https:\/\/(?:www\.)?(mangadex\.org\/title\/\d+\/[^\/]+)(\/.+)?$/", $link, $m))
            return 'https://'.$m[1];

        return null;
    }


    public function getLatestVersion(string $link): ?object
    {
        $dom = new Dom();
        try {
            $dom->loadFromUrl($link, [], new CustomCurl());
        } catch (CurlException $e) {
            return null;
        }
        
        $name = @trim($dom->find('h6.card-header span.mx-1')[0]->text);
        if (empty($name))
            return null;

        $chapter_divs = $dom->find('div.chapter-container div.chapter-row');
        $latest_chapter_dt = '';
        foreach ($chapter_divs as $i => $chapter_div) {
            if ($i == 0)
                continue;
            $dt = $chapter_div->find('div.order-lg-8')->getAttribute('title');
            if ($latest_chapter_dt > $dt)
                continue;
            $latest_chapter_dt = $dt;
            $latest_chapter_name = $chapter_div->find('div.order-lg-2 a')->text;
            $latest_chapter_lang = $chapter_div->find('div.order-lg-4 span.flag')->getAttribute('title');
        }

        // completed
        $completed = false;
        $head_table_rows = $dom->find('div.card-body div.edit div.col-xl-9 div.border-top');
        //var_dump(count($head_table_rows));
        foreach ($head_table_rows as $head_table_row) {
            //var_dump($head_table_row->find('div.col-xl-2'));
            $row_left = $head_table_row->find('div.col-xl-2')->text;
            if ($row_left !== 'Pub. status:')
                continue;
            $row_right = $head_table_row->find('div.col-xl-10')->text;
            if ($row_right === 'Completed')
                $completed = true;
        }

        unset($dom);

        if (empty($latest_chapter_name))
            return null;

        $mark = (object)[
            'name'      => $name,
            'last_ep'   => $latest_chapter_name,
            'lang'      => $latest_chapter_lang,
        ];
        if ($completed)
            $mark->completed = 1;

        return (object)[
            'mark'  => $mark,
            'hash'  => sha1($latest_chapter_name.':'.$latest_chapter_lang.':'.$latest_chapter_dt),
        ];
    }
}
