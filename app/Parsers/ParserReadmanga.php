<?php

namespace App\Parsers;

use App\Parsers\Curl\CustomCurl;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Exceptions\CurlException;

class ParserReadmanga extends AbstractParser
{
    public static $manifest = [
        'host'  => [
            'readmanga.live' => 'https://readmanga.live/<NAME>',
            'mintmanga.live' => 'https://mintmanga.live/<NAME>',
        ],
        'type'  => 'manga',
    ];

    public static function verifyLink(string $link): ?string
    {
        $link = trim($link);

        if (preg_match("/^https?:\/\/(readmanga\.live\/[^\/]+|mintmanga\.live\/[^\/]+)(?:\/vol\d+\/\d+(?:\?.*)?)?$/", $link, $m))
            return 'https://'.$m[1];

        return null;
    }


    public function getLatestVersion(string $link): ?object
    {
        $dom = new Dom();
        try {
            $dom->loadFromUrl($link, [], new CustomCurl());
        } catch (CurlException $e) {
            return null;
        }

        $name = @trim($dom->find('h1.names span.name')[0]->text);
        if (empty($name))
            return null;

        $latest_chapter_name = @trim($dom->find('div.chapters-link table.table td a')[0]->text);
        if (empty($latest_chapter_name))
            $latest_chapter_name = '';
        else
            $latest_chapter_name = trim(str_replace($name, '', $latest_chapter_name));

        $completed = false;
        foreach ($dom->find('div.subject-meta p') as $p) {
            if (trim($p->find('b')->text) == 'Перевод:') {
                $translation = trim($p->text);
                if ($translation == 'завершен')
                    $completed = true;
                break;
            }
        }

        unset($dom);

        $mark = (object)[
            'name'      => $name,
            'last_ep'   => $latest_chapter_name,
        ];
        if ($completed)
            $mark->completed = 1;

        return (object)[
            'mark'  => $mark,
            'hash'  => sha1($latest_chapter_name),
        ];
    }
}
