<?php

namespace App\Parsers;

use App\Parsers\Traits\IsStatic;
use App\Parsers\AbstractParser;

/**
 * This is a ParserFactory, gets a suitable parser by link.
 * But also has some methods manipulating all parsers
 */
final class ParserFactory
{
    use IsStatic;

    private static $instance;

    /**
     * @var array Array of all available parsers
     */
    private $parsers = [];

    /**
     * Gets the instance
     * @return self Part of a singleton template, but also loads the parsers list
     */
    public static function getInstance(): self
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
        }
        self::$instance->loadParsersList();
        return self::$instance;
    }

    /**
     * Gets the list of all available parsers. Loads it into $this->parsers.
     */
    private function loadParsersList(): void
    {
        $files = scandir(__DIR__);

        if ($files === false)
            return;

        foreach ($files as $file) {
            if (!is_file(__DIR__.'/'.$file))
                continue;
            if (preg_match("/^(Parser\w+)\.php$/", $file, $m)) {
                $class = __NAMESPACE__.'\\'.$m[1];
                if (class_exists($class) && !empty($class::$manifest['host']))
                    $this->parsers[] = $class;
            }
        }
    }

    /**
     * Gets the parser suitable for the link
     * @param string $link The link to search a parser for
     * @return AbstractParser A parser, that can parse this link. Or false if it wasn't found
     */
    public function getParser(string $link): ?AbstractParser
    {
        foreach ($this->parsers as $parser) {
            if ($parser::verifyLink($link))
                return $parser::getInstance();
        }

        return null;
    }

    /**
     * Checks wether there is a parser for the link
     * @param string $link The link to search a parser for
     * @return object Has two params:
     *      "isAllowed" bool - Is there is a parser for the link
     *      "correctLink" string - if isAllowed is true: contents a corrected link
     *          if isAllowed is false: contents a correct template of a link
     *          or an empty string if no parser for this site found
     */
    public function isLinkAllowed(string $link): object
    {
        $result = (object)[
            'isAllowed'    => false,
            'correctLink'  => '',
        ];

        $host = strtolower(parse_url($link, PHP_URL_HOST));
        $host = preg_replace("/^www\./", '', $host);
        foreach ($this->parsers as $parser) {
            if (!empty($parser::$manifest['host'][$host])) {
                $correctFormat = $parser::$manifest['host'][$host];
                $correctLink = $parser::verifyLink($link);
                if ($correctLink) {
                    $result->isAllowed = true;
                    $result->correctLink = $correctLink;
                    return $result;
                } else {
                    $result->correctLink = $correctFormat;
                    return $result;
                }
            }
        }

        return $result;
    }

    /**
     * Gets manifests (descriptions) of all parsers grouping them by site type (anime/torrent/etc)
     * The purpose - to show them to the user
     * @return array All manifests
     *      example: [
     *          'torrent'   => [
     *              'rutracker.org' => 'https://rutracker.org/forum/viewtopic.php?t=<ID>'
     *          ]
     *      ]
     */
    public function getAllManifests(): array
    {
        $manifests = [];
        foreach ($this->parsers as $parser) {
            if (!empty($parser::$manifest['host']) && !empty($parser::$manifest['type'])) {
                $type = $parser::$manifest['type'];
                if (!isset($manifests[$type]))
                    $manifests[$type] = [];
                $manifests[$type] = array_merge($manifests[$type], array_keys($parser::$manifest['host']));
            }
        }
        ksort($manifests);
        foreach ($manifests as $key => $val)
            sort($manifests[$key]);

        return $manifests;
    }
}
