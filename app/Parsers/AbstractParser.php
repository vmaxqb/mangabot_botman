<?php
namespace App\Parsers;

use App\Parsers\Traits\IsSingleton;

/**
 * This class is a basis for all parsers
 * It checks wether the link is suitable for the parser and parses it
 */
abstract class AbstractParser
{
    use IsSingleton;

    /**
     * @var array includes the description of hosts it can parse, correct link pattern, type of site 
     * example:
     *  [
     *      'host'  => 
     *          ['example.com'  => 'https://example.com/<ID>/<NAME>'],
     *      'type'  => 'torrent', // 'manga', 'anime', etc
     *  ]
     */
    public static $manifest = [];

    /**
     * Verifies wether the link can be parsed by this parser, returns correct link if so
     * Returns null otherwise
     * @param string $link The link we are checking
     * @return string Correct link or null if it cannot be parsed
     */
    abstract public static function verifyLink(string $link): ?string;

    /**
     * Parses the site and retrieves the data
     * @param string $link The link to parse
     * @return object
     *      Null if site is unavailable or unparsable
     *      Object otherwise. This object MUST have "hash" and "mark" params:
     *      "hash" param - string up to 40 chars, basically sha1 of something unique for this serie
     *      "mark" param - object:
     *          MUST have "name" param - string, name of the serie,
     *          CAN have "last_ep" param - string, name of the last episode
     *          CAN have "completed" param - int, 0|1, 1 if serie is completed and there is no need to parse again
     *          CAN have any other params, they will have no particular effect
     */
    abstract public function getLatestVersion(string $link): ?object;
}
