<?php

namespace App\Parsers\Traits;

/**
 * Trait of a singleton template
 */
trait IsSingleton
{
    use IsStatic;

    /**
     * Can have multiple children, but only one object of each class
     * @var array of instances 
     */
    private static $instances = [];

    public static function getInstance(): self
    {
        $class = get_called_class();
        if (!isset(self::$instances[$class]))
            self::$instances[$class] = new $class();
        return self::$instances[$class];
    }
}
