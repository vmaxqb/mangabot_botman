<?php

namespace App\Parsers\Traits;

/**
 * This trait is used for classes that are not supposed to be created (using "new" command) by outside force
 * Used in singletons or classes with no non-static functions
 */
trait IsStatic
{
    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __sleep()
    {
    }

    private function __wakeup()
    {
    }
}
