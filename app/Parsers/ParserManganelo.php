<?php

namespace App\Parsers;

use App\Parsers\Curl\CustomCurl;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Exceptions\CurlException;

class ParserManganelo extends AbstractParser
{
    public static $manifest = [
        'host'  => [
            'manganelo.com'     => 'https://manganelo.com/manga/<NAME>',
            'mangakakalot.com'  => 'https://mangakakalot.com/manga/<NAME>',
        ],
        'type'  => 'manga',
    ];

    public static function verifyLink(string $link): ?string
    {
        $link = trim($link);

        if (preg_match("/^https?:\/\/((?:manganelo|mangakakalot)\.com\/manga\/[^\/]+)$/", $link, $m))
            return 'https://'.$m[1];

        if (preg_match("/^https?:\/\/(manganelo|mangakakalot)\.com\/chapter\/([^\/]+)\/[^\/]+$/", $link, $m))
            return 'https://'.$m[1].'.com/manga/'.$m[2];

        return null;
    }


    public function getLatestVersion(string $link): ?object
    {
        $dom = new Dom();
        try {
            $dom->loadFromUrl($link, [], new CustomCurl());
        } catch (CurlException $e) {
            return null;
        }

        $name = @trim($dom->find('h1')[0]->text);
        if (empty($name))
            return null;

        $latest_chapter_name = $dom->find('div.panel-story-chapter-list a.chapter-name')[0]->text;

        $completed = false;
        foreach ($dom->find('table.variations-tableInfo td.table-value') as $td) {
            if ($td->text == 'Completed')
                $completed = true;
        }

        unset($dom);

        $mark = (object)[
            'name'      => $name,
            'last_ep'   => $latest_chapter_name,
        ];
        if ($completed)
            $mark->completed = 1;

        return (object)[
            'mark'  => $mark,
            'hash'  => sha1($latest_chapter_name),
        ];
    }
}
