<?php

namespace App\Http\Controllers;

use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;
use App\Conversations\ExampleConversation;
use App\Link;
use App\Parsers\ParserFactory;
use App\Suggest;
use App\UserLink;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

class BotManController extends Controller
{
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');

        $botman->listen();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tinker()
    {
        return view('tinker');
    }

    /**
     * Loaded through routes/botman.php
     * @param  BotMan $bot
     */
    public function startConversation(BotMan $bot)
    {
        $bot->startConversation(new ExampleConversation());
    }

    /**
     * /start command
     * @param  BotMan $bot
     */
    public function convStart(BotMan $bot)
    {
        $bot->reply(__('messages.START_MSG'));
    }

    /**
     * /lang_xx command
     * @param  BotMan $bot
     * @param  string $lang - new language for this user
     */
    public function convLangChange(BotMan $bot, string $lang)
    {
        if (!in_array($lang, config('botman.config.available_langs'))) {
            $bot->reply(__('messages.UNKNOWN_LANG'));
            return;
        }
        
        $user = Config::get('current_user');
        $user->lang = $lang;
        $user->save();

        App::setLocale($lang);
        $bot->reply(__('messages.LANG_SET'));
    }

    /**
     * /about command
     * @param  BotMan $bot
     */
    public function convAbout(BotMan $bot)
    {
        $bot->reply(__('messages.ABOUT_MSG'));
    }

    /**
     * /sites command
     * @param  BotMan $bot
     */
    public function convSites(BotMan $bot)
    {
        $parserFactory = ParserFactory::getInstance();
        $mainfests = $parserFactory->getAllManifests();
        foreach ($mainfests as $type => $sites)
            $answerParts[] = '<i>'.ucwords($type).':</i>'."\n".implode("\n", $sites);

        $bot->reply(implode("\n\n", $answerParts));
    }

    /**
     * We recieved a link and have to add it
     * @param  BotMan $bot
     * @param  string $message - lots of links
     */
    public function convAddLinks(BotMan $bot, string $message)
    {
        $user = Config::get('current_user');
        $parserFactory = ParserFactory::getInstance();
        
        $parts = preg_split("/\s+/", $message);
        $parts = array_unique($parts);
        $answer_parts = [];
        foreach ($parts as $linkStr) {
            $linkStr = trim($linkStr);
            if (!preg_match("/^https?:\/\//", $linkStr))
                continue;
            if (!filter_var($linkStr, FILTER_VALIDATE_URL))
                continue;
            $linkAllowed = $parserFactory->isLinkAllowed($linkStr);
            if ($linkAllowed->isAllowed) {
                $linkStr = $linkAllowed->correctLink;
                $link = Link::firstOrCreate(['link' => $linkStr]);
                UserLink::firstOrCreate([
                    'user_id' => $user->user_id,
                    'link_id' => $link->link_id,
                ]);

                $answer_parts[] = $linkStr."\n".__('messages.LINK_ADDED', ['link_id' => $link->link_id]);
            } elseif (!empty($linkAllowed->correctLink))
                $answer_parts[] = $linkStr."\n".__('messages.LINK_INCORRECT_FORMAT', ['link_format' => htmlspecialchars($linkAllowed->correctLink)]);
            else {
                $suggest = new Suggest();
                $suggest->link = $linkStr;
                $suggest->user_id = $user->user_id;
                $suggest->save();
                $answer_parts[] = $linkStr."\n".__('messages.LINK_UNKNOWN');
            }
        }

        if (empty($answer_parts))
            $answer_parts[] = __('messages.LINK_UNKNOWN');

        $options = [];
        if ($user->platform === 'tg')
            $options = ['disable_web_page_preview' => true];

        $bot->reply(implode("\n\n", $answer_parts), $options);
    }

    /**
     * /del_ul command
     * @param  BotMan $bot
     * @param  int $link_id
     */
    public function convDeleteLink(BotMan $bot, int $linkId)
    {
        $user = Config::get('current_user');
        $user->userLinks()->where('link_id', $linkId)->delete();

        $bot->reply(__('messages.DELETED'));
    }

    /**
     * /list command
     * @param  BotMan $bot
     * @param  int $link_id
     */
    public function convListLinks(BotMan $bot)
    {
        $user = Config::get('current_user');

        $answer_parts = [];
        foreach ($user->links()->orderBy('link')->get() as $link) {
            $lastVersionMark = @json_decode($link->last_version_mark);
            $linkName = !empty($lastVersionMark->name) ? $lastVersionMark->name : $link->link;
            $answer_parts[] = 
                __('messages.LINK', ['link' => $link->link, 'name' => $linkName])."\n".
                __('messages.DELETE_LINK', ['link_id' => $link->link_id])
            ;
        }

        $options = [];
        if ($user->platform === 'tg')
            $options = ['disable_web_page_preview' => true];

        $bot->reply(implode("\n\n", $answer_parts), $options);
    }

    /**
     * Fallback
     * @param  BotMan $bot
     */
    public function convFallback(BotMan $bot)
    {
        $bot->reply(__('messages.UNKNOWN_COMMAND'));
    }
    
}
