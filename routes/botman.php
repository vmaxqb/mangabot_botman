<?php
use App\Http\Controllers\BotManController;
use App\Http\Middleware\RegisterUser;

$botman = resolve('botman');

$userRegisterMiddleware = new RegisterUser();
$botman->middleware->received($userRegisterMiddleware);
$botman->middleware->sending($userRegisterMiddleware);
/*
$botman->hears('Hi', function ($bot) {
    $bot->reply('Hello!');
});
$botman->hears('Start conversation', BotManController::class.'@startConversation');
*/
$botman->hears('^/start', BotManController::class.'@convStart');
$botman->hears('^/lang_{lang}', BotManController::class.'@convLangChange');
$botman->hears('^/about', BotManController::class.'@convAbout');
$botman->hears('^/sites', BotManController::class.'@convSites');
$botman->hears('^\s*(https?://.+)$', BotManController::class.'@convAddLinks');
$botman->hears('^/del_ul{linkId}', BotManController::class.'@convDeleteLink');
$botman->hears('^/list', BotManController::class.'@convListLinks');

$botman->fallback(BotManController::class.'@convFallback');
