<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->bigIncrements('link_id');
            $table->string('link', 2048)->nullable()->default(null);
            $table->string('last_version_mark', 5000)->nullable()->default(null);
            $table->string('last_version_hash', 40)->nullable()->default(null);
            $table->timestamp('last_version_dt')->nullable()->default(null);
            $table->tinyInteger('completed')->default(0);
            $table->timestamp('last_checked_dt')->nullable()->default(null);
        });

        DB::statement('CREATE INDEX link ON links (link(100));');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
