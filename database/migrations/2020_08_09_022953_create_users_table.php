<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('user_id');
            $table->enum('platform', ['tg', 'mail']);
            $table->string('ext_id', 100);
            $table->tinyInteger('tz_data')->default(5);
            $table->string('lang', 5)->default('ru');
            $table->unique(['platform', 'ext_id'], 'platform_ext_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
