<?php

return [
    'NEW_EPISODE_OUT'       => "New episode is out",
    'END_EPISODE'           => "That's the end episode",
    'EPISODE_LANG'          => "Episode language",
    'LINK_ADDED'            => "Added. Want to delete? Click /del_ul:link_id",
    'LINK_INCORRECT_FORMAT' => "Not added, sorry. Correct format is: :link_format",
    'LINK_UNKNOWN'          => "Sorry, this web site is not supported yet",
    'NO_LINKS_DETECTED'     => "No links detected",
    'DELETED'               => "Deleted",
    'DELETE_LINK'           => "Want to delete? Click /del_ul:link_id",
    'START_MSG'             => "Hello dear friend. Do you want to be notified of new series of your favorite anime / manga / films / etc? I will do that. Just give me the link, and I will notify you when the new episodes are out. Say /about if you want to know more about me\n\n/lang_ru - Переключить язык на русский",
    'UNKNOWN_COMMAND'       => "Unknown command. Sorry about that",
    'ABOUT_MSG'             => "This bot watches all your anime / manga / torrents / films / books / others sites for new series. If a new serie is out, this bot will inform you about it\n\n<i>Commands:</i>\njust the link - Adds the link to your watchlist\n/list - Get the list of all links you are watching\n/sites - Get all the sites the bot can work with\n/start - Start having fun with me!\n/about - This text\nIf you want to delete some link from your watchlist, just write that link and the bot will answer you what command should you use\n\n/lang_en - Switch language to english\n\n/lang_ru - Переключить язык на русский\n\nAny suggestions? Fell free to write to @stayon",
    'LANG_SET'              => "Language set to english",
    'UNKNOWN_LANG'          => "Sorry, this language is not available",
    'LINK'                  => "<a href=\":link\">:name</a>",
];
